# Changelog
Make sure to update this file for each merge into develop, otherwise the build fails.
The build relies on the latest version in this file.
Latest versions must be at the top!

## [1.1.0] - 2023-05-08
 - add var to rebuild only one image
 - add var for job image
 - decrease cicd-utils image size

## [1.0.0] - 2023-03-15
 - init repo
 - add utils image
 - ad gradle, lambda, amazoncorretto images




