# gitlab cicd-images

This repo contains necessary docker base and build images

current images list:
- amazoncorretto:11-alpine
- lambda-builder:python-3.9
- gradle-builder:7.6-jdk-11
- gradle-builder:6.6-jdk-11
- docker:docker-dind
- docker:docker

and we have next global variables:
CICD_IMAGES_REGISTRY = registry.gitlab.com/mbelousov7-devops/cicd-images

## how to use images in gitlab pipeleines

in component project configure .gitlab-ci.yml next images variables like in examples:

``` yaml
  PYTHON_IMAGE: $CICD_IMAGES_REGISTRY/lambda-builder:python-3.9
  GRADLE_IMAGE: $CICD_IMAGES_REGISTRY/gradle-builder:6.6-jdk-11
``` 

## how to use images in Dokerfiles

in docker file add *ARG CICD_IMAGES_REGISTRY*, and use this variable in *FROM* instruction like next
``` Dockerfile
ARG CICD_IMAGES_REGISTRY
FROM $CICD_IMAGES_REGISTRY/amazoncorretto:11-alpine
```
