#!/usr/bin/env bash

ROOT_DIR="/ci-utils"

SCRIPT_NAME="$1"
shift

# check if it's a Python script
if [ -f "${ROOT_DIR}/${SCRIPT_NAME}.py" ]; then
    python3 ${ROOT_DIR}/${SCRIPT_NAME}.py "$@"

# if not, then check if it's a shell script
elif [ -f "${ROOT_DIR}/${SCRIPT_NAME}.sh" ]; then
    ${ROOT_DIR}/${SCRIPT_NAME}.sh "$@"

# otherwise fail
else
    echo "script '$SCRIPT_NAME' not found"
    exit 1
fi
