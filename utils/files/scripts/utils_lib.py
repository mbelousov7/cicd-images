import semver
import re
import sys
import os

def get_latest_version(versions):
    latest_version = None
    for version in versions:
        if latest_version is None or semver.compare(version, latest_version) > 0:
            latest_version = version

    return latest_version


def get_latest_version_from_tags(tags):
    latest_version = None
    for tag in tags:
        if re.fullmatch("v[0-9]+\.[0-9]+\.[0-9]+", tag):
            without_v = tag[1:]
            if latest_version is None or semver.compare(without_v, latest_version) > 0:
                latest_version = without_v

    return latest_version


def exit_with_error(error):
    COLOR_RED = '\033[91m'
    COLOR_END = '\033[0m'

    print("\n\n")
    print(COLOR_RED + "--- error ---" + COLOR_END)
    print(COLOR_RED + error + COLOR_END)

    sys.exit(1)

def log_file_path(file):
    return f"{file} (abs: {os.path.abspath(file)})"