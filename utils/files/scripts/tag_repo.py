import gitlab
import os

if __name__ == "__main__":
    # note: this is using the group access token so that the pipeline that gets triggered on this tag (that does nothing)
    # doesn't fail because it can't access cicd-libs
    gl = gitlab.Gitlab(url=os.environ.get('CI_SERVER_URL'), private_token=os.environ.get('GROUP_ACCESS_TOKEN'))
    project = gl.projects.get(os.environ.get('CI_PROJECT_ID'))

    tag_name = 'v' + os.environ.get('COMPONENT_VERSION')
    commit_sha = os.environ.get('CI_COMMIT_SHA')
    is_ignore_files_only = os.environ.get('IS_IGNORE_FILES_ONLY')

    print(f"tagging {commit_sha} with tag {tag_name}")

    try:
        project.tags.create({'tag_name': tag_name, 'ref': commit_sha})
    except (gitlab.exceptions.GitlabCreateError) as e:
        if is_ignore_files_only:
            print(f"were changed only ignored files so, delete current tag:{tag_name} and create new with the same value")
            project.tags.delete(id=tag_name)
            project.tags.create({'tag_name': tag_name, 'ref': commit_sha})
        else:
           print(f"{e}")