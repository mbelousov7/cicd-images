import os
import sys

if __name__ == "__main__":
    vars_to_check = sys.argv[1:]

    missing_vars = []
    for var in vars_to_check:
        value = os.environ.get(var)
        if value is None or len(value.strip()) == 0:
            missing_vars.append(var)

    if len(missing_vars) > 0:
        print(f"missing required environment variables {', '.join(missing_vars)}")
        sys.exit(1)

