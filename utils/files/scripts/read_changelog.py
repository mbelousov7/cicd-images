import os
import argparse
import re
import semver
import gitlab
import json
from utils_lib import exit_with_error, log_file_path, get_latest_version


def __read_versions_from_tags(project):
    versions = []
    for tag_meta in project.tags.list():
        # tag_meta has multiple properties like the commit hash, author and so on
        # we only care about the tag name
        tag = tag_meta.name

        # check if tag is version tag
        if re.fullmatch("v[0-9]+\.[0-9]+\.[0-9]+", tag):
            without_v = tag[1:]
            versions.append(without_v)


    print(f"Read all version tags: {versions}")
    return versions


def __get_latest_version_from_tags(versions):
    latest_version = get_latest_version(versions)
    print(f"latest tag: {latest_version}")

    return latest_version


def __read_changelog_versions(changelog_file):
    if not os.path.isfile(changelog_file):
        exit_with_error(f"error: changelog file {log_file_path(changelog_file)} does not exist, or it's not a file")

    regex = r'^## \[(\d+\.\d+\.\d+)\] - \d+-\d+-\d+ *'

    versions = []

    print(f"opening {log_file_path(changelog_file)}")
    with open(changelog_file, "r") as fp:
        for line in fp.readlines():
            m = re.match(regex, line)
            if m is not None:
                versions.append(m.group(1))

    print(f"read {len(versions)} versions: {versions}")

    if len(versions) > len(set(versions)):
        exit_with_error("error: changelog versions are not unique")

    if len(versions) == 0:
        exit_with_error("error: no versions found in the changelog")

    return versions


def check_if_changelog_was_updated(project, changelog_file, is_hotfix, is_ignore_files_only):

    versions_from_tags = __read_versions_from_tags(project)
    latest_tag_version = __get_latest_version_from_tags(versions_from_tags)

    latest_changelog_version = __read_changelog_versions(changelog_file)[0]

    print(f"all versions from tags: {versions_from_tags}")
    print(f"latest version from tags: {latest_tag_version}")
    print(f"latest version from changelog: {latest_changelog_version}")

    if latest_tag_version == latest_changelog_version and is_ignore_files_only == False:
        exit_with_error("error: changelog version was not updated")

    if is_hotfix:
        # make sure none of the existing versions match the changelog version
        if latest_changelog_version in versions_from_tags:
            exit_with_error(f"error: changelog version must be unique")

        print("HOTFIX: won't enforce changelog version to be greater than the latest tagged version")
    else:
        if latest_tag_version is not None and semver.compare(latest_changelog_version, latest_tag_version) <= 0 and is_ignore_files_only == False:
            exit_with_error("error: changelog version must be greater than the latest tagged version")

    if is_ignore_files_only:
        print("Only ignored files were changed - version could be the same")
    else:
        print("changelog version ok")

    return latest_changelog_version


if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument('--ignore-files', nargs='+', required=False)
    args = parser.parse_args()
    changelog_file = 'CHANGELOG.md'
    
    is_hotfix = os.environ.get('IS_HOTFIX') != None

    # components that have maven deploy should set this variable in their .gitlab-ci.yml file to 'true'
    has_maven_deploy = os.environ.get('HAS_MAVEN_DEPLOY', 'false').lower() == 'true'

    gl = gitlab.Gitlab(url=os.environ.get('CI_SERVER_URL'), private_token=os.environ.get('GROUP_ACCESS_TOKEN'))
    project = gl.projects.get(os.environ.get('CI_PROJECT_ID'))


    # this variable is only set if the pipeline is running for a merge request
    # the IID is not a typo, it's the internal ID, unique within a project (repository)
    mr_id = os.environ.get('CI_MERGE_REQUEST_IID')
    commit_id = os.environ.get('CI_COMMIT_SHA')

    #if pipeline run from develop branch:
    if mr_id is None:
        commit = project.commits.get(commit_id)
        #diff = commit.diff()
        changed_files_list = [d['new_path'] for d in commit.diff()]
        print(f"commmit diff: {changed_files_list}")
    #if pipeline run from MR event
    else:
        mr = project.mergerequests.get(mr_id)
        changes = mr.changes()
        changed_files_list = [d['new_path'] for d in changes['changes']]
        print(f"MR changed files: {changed_files_list}")

    print(f"ignore files: {args.ignore_files}")

    #if ignore_files parameter not provided 
    if args.ignore_files is None:
        is_ignore_files_only = False
        print(f"There are no ignored files, ignore_files_only:{is_ignore_files_only}")
    #if ignore_files parameter provided then compare this list with changed files list
    else:
        is_ignore_files_only = all(item in args.ignore_files for item in changed_files_list)
        print(f"Have we only changed ignored files: {is_ignore_files_only}")

    # If were changed only ignored files we rebuild and publish all artifacts with the current version from changelog
    latest_changelog_version = check_if_changelog_was_updated(project, changelog_file, is_hotfix, is_ignore_files_only)
    print(f"latest version from changelog: {latest_changelog_version}")

    if mr_id is None:
        version_env = f"COMPONENT_VERSION={latest_changelog_version}\nIS_IGNORE_FILES_ONLY={is_ignore_files_only}"
    else:
        if has_maven_deploy:
            version_env = f"COMPONENT_VERSION={latest_changelog_version}-mr{mr_id}-SNAPSHOT"
        else:
            version_env = f"COMPONENT_VERSION={latest_changelog_version}-mr{mr_id}"

    print(f"exporting environment: {version_env}")

    with open("version.env", "w") as fp:
        fp.write(version_env)
