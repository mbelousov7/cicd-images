import json
import os
import sys
import argparse
import gitlab
from utils_lib import get_latest_version_from_tags, exit_with_error


def placeholder_from_component_name(component_name):
    return component_name.upper().replace('-', '_') + "_VERSION"


def map_versions_to_placeholders(components_versions):
    output = {}

    for component_name in components_versions:
        output[placeholder_from_component_name(component_name)] = components_versions[component_name]

    return output


def replace_versions(placeholders_map, files_to_replace_in):
    if files_to_replace_in is None:
        return

    for file_name in files_to_replace_in:
        stripped_file_name = file_name.strip()
        if len(stripped_file_name) > 0:
            filedata = None
            print("replacing in " + stripped_file_name)
            with open(stripped_file_name, "r") as file_read:
                filedata = file_read.read()

            for key in placeholders_map:
                filedata = filedata.replace("[[!" + key + "]]", placeholders_map[key])

            with open(stripped_file_name, "w") as file_write:
                file_write.write(filedata)


def run_in_component_mode(args):
    """
    Replaces [[!VERSION]] with the provided version in the provided files
    """
    print(f"running in single component mode")
    print(f"version: {args.version}")
    print(f"replacing in files: {args.files}")

    replace_versions({"VERSION": args.version}, args.files)


def run_in_stack_mode(args):
    print(f"running in stack mode")
    print(f"components: {args.components}")
    print(f"stack: {args.stack}")
    print(f"replacing in files: {args.files}")

    gl = gitlab.Gitlab(url=args.gitlab_url, private_token=args.gitlab_token)

    # component name -> latest version
    versions_map = {}

    if args.components is None:
        args.components = []

    for component in args.components:
        print(f"Fetching latest version for {args.gitlab_namespace}/{component}")

        project = gl.projects.get(f"{args.gitlab_namespace}/{component}")

        tag_names = []
        for tag_meta in project.tags.list():
            # tag_meta has multiple properties like the commit hash, author and so on
            # we only care about the tag name
            tag_names.append(tag_meta.name)

        latest_version = get_latest_version_from_tags(tag_names)
        if latest_version is None:
            exit_with_error(f"error: couldn't determine latest version for component {component}")

        versions_map[component] = latest_version

    for stack in args.stack:
        print(f"Fetching latest version for {args.gitlab_namespace}/{stack}")

        project = gl.projects.get(f"{args.gitlab_namespace}/{stack}")

        tag_names = []
        for tag_meta in project.tags.list():
            # tag_meta has multiple properties like the commit hash, author and so on
            # we only care about the tag name
            tag_names.append(tag_meta.name)

        latest_version = get_latest_version_from_tags(tag_names)
        if latest_version is None:
            latest_version = os.environ.get('CI_DEFAULT_BRANCH')

        versions_map[stack] = latest_version

    print(f"latest versions for components {versions_map}")

    placeholders_map = map_versions_to_placeholders(versions_map)
    replace_versions(placeholders_map, args.files)

    if args.write_versions_to_file is not None:
        with open(args.write_versions_to_file, "w") as fp:
            for e in versions_map.items():
                fp.write(f"{e[0]}: {e[1]}\n")

    if args.write_versions_to_json is not None:
        with open(args.write_versions_to_json, "w") as fp:
            fp.write(json.dumps(versions_map, sort_keys=True, indent=2))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # there are two modes of operation:

    # component mode
    # --------------
    # replaces the [[!VERSION]] placeholder with the provided --version in the provided --files
    mode_component = subparsers.add_parser('component')
    mode_component.add_argument('--version', required=True)
    mode_component.add_argument('--files', nargs='+', required=True)
    mode_component.set_defaults(func=run_in_component_mode)

    # stack mode
    # ----------
    # 1. fetches the tags for each repository (component) using the GitLab API
    # 2. determines which tag is the latest version for each, using semver comparisons
    # 3. replaces the [[!{COMPONENT_NAME}_VERSION]] placeholders with the component's latest version
    mode_stack = subparsers.add_parser('stack')
    mode_stack.add_argument('--stack', nargs='+', required=True)
    mode_stack.add_argument('--components', nargs='+', required=False)
    mode_stack.add_argument('--files', nargs='+', required=False)
    mode_stack.add_argument('--write-versions-to-file', required=False, default=None)
    mode_stack.add_argument('--write-versions-to-json', required=False, default=None)
    mode_stack.add_argument('--gitlab-url', required=True)
    mode_stack.add_argument('--gitlab-token', required=True)
    mode_stack.add_argument('--gitlab-namespace', required=True) # the GitLab group in which the projects exist
    mode_stack.set_defaults(func=run_in_stack_mode)

    args = parser.parse_args()
    args.func(args)
