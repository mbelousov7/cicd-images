import os
import gitlab
import json

def __get_stdout(command: str):
    with os.popen(command) as i:
        return i.read()

if __name__ == "__main__":
    jwt = os.environ.get('CI_JOB_JWT')
    project_ns = os.environ.get('CI_PROJECT_NAMESPACE')

    aws_account_id_ppe = os.environ.get('PPE_AWS_ACCOUNT_ID')
    aws_account_id_prod = os.environ.get('PROD_AWS_ACCOUNT_ID')

    vault_token = __get_stdout(f"vault write -field=token auth/jwt/login role={project_ns}-aws jwt={jwt}")

    os.environ['VAULT_TOKEN'] = vault_token

    ppe_credentials = json.loads(__get_stdout(f"vault read -format=json gitlab/{project_ns}/aws/creds/{aws_account_id_ppe}-a{project_ns}-developer"))
    prod_credentials = json.loads(__get_stdout(f"vault read -format=json gitlab/{project_ns}/aws/creds/{aws_account_id_prod}-a{project_ns}-developer"))

    gl = gitlab.Gitlab(url=os.environ.get('CI_SERVER_URL'), private_token=os.environ.get('PROJECT_ACCESS_TOKEN'))
    project = gl.projects.get(os.environ.get('CI_PROJECT_ID'))

    def update_var(name, value):
        var = project.variables.get(name)
        var.value = value
        var.save()

    def update_aws_vars(prefix, credentials):
        data = credentials['data']
        update_var(f"{prefix}_AWS_ACCESS_KEY_ID", data['access_key'])
        update_var(f"{prefix}_AWS_SESSION_TOKEN", data['security_token'])
        update_var(f"{prefix}_AWS_SECRET_ACCESS_KEY", data['secret_key'])

    update_aws_vars('PPE', ppe_credentials)
    update_aws_vars('PROD', prod_credentials)
