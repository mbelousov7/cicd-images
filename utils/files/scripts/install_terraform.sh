#!/bin/bash

TERRAFORM_VERSION="$1" # first arg

cd /tmp/

curl "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" --output terraform.zip
unzip terraform.zip -d terraform
rm terraform.zip
mv ./terraform/terraform /usr/bin/terraform
rm -r ./terraform
chmod +x /usr/bin/terraform

cd -
